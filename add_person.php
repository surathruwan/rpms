
<!-- add_person.php -->
<html>
<head></head>
<!-- Styles Sheet & Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="scripts/validate.js"></script>
<body>

	<div class="col-md-12"> <!-- Start of Main Div -->
			
			<div class="col-md-12"> <!-- Start of Heading -->
				<!-- <h3 style="text-align:center">Welcome Page</h3> -->
			</div> <!-- End of Heading -->

			<div class="col-md-12" > <!-- Start of Navigation content -->
					<div class="col-md-2 "></div>
					<div class="col-md-2 item">
					  
					</div>
					<div class="col-md-2 item">
					  <a href="Add_person.php">
						<img src="images/add-user.png" alt="" width="45%" height="auto">
					  </a>
  						<div class="item-overlay top"></div>
					</div>
					<div class="col-md-2 item">
					  <a href="view.php">
						<img src="images/search-user.png" alt="" width="45%" height="auto">
                      </a>
  						<div class="item-overlay top"></div>
					</div>
					<div class="col-md-2 item">
             
					</div>
            <div class="col-md-2 "></div>

			</div> <!-- End of Navigation content -->

			<div class="col-md-12"> <!-- Start of Add Person Form -->
				<div class="col-md-3"></div>
				<div class="col-md-6">
				
				<form method="post" id="person_form" name="person_form" enctype="multipart/form-data">
					<h3 style="text-align:center">Add Person</h3>
					<div class="form-group">
						<span id="uploaded_image"></span>
						<br>
	                    <label>Select File </label>
	                    <input type="file" name="image_upload" id="image_upload"  />
	                    <input type="hidden" name="image_path" id="image_path"  />
	                    <br />
	                    
	                </div>
	                <div id="response"></div>
	                
	                <div class="form-group">
	                    <label>First Name</label>
	                    <input type="text" name="txtfname" id="txtfname" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Last Name</label>
	                    <input type="text" name="txtlname" id="txtlname" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Short Name</label>
	                    <input type="text" name="txtshortname" id="txtshortname" class="form-control" required/>
	                </div>

	                <div class="form-check form-check-inline">
	                	 <label>Gender</label><br>
						 <input class="form-check-input" type="radio" name="rdogender" id="male" value="Male">
						 <label class="form-check-label" for="male">Male</label>
					
						 <input class="form-check-input" type="radio" name="rdogender" id="female" value="Female">
						 <label class="form-check-label" for="female">Female</label>
					</div>

	                <div class="form-group">
	                    <label>Email</label>
	                    <input type="email" name="txtemail" id="txtemail" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Phone</label>
	                    <input type="phone" name="txtPhone" id="txtPhone" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label for="cmbService">Service</label>
	                    <select class="form-control" id="cmbService" name="cmbService" >
	                    </select>
	                </div>

	                <div class="form-group">
	                    <label for="cmbMedium">Medium</label>
	                    <select class="form-control" id="cmbMedium" name="cmbMedium"  >
	                    </select>
	                </div>

	                <div class="form-group">
	                    <label>Grade</label>
	                    <input type="text" name="txtGrade" id="txtGrade" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Designation</label>
	                    <input type="text" name="txtDesignation" id="txtDesignation" class="form-control" required/>
	                </div>

	                <div class="form-group ">
	                    <label for="cmbWorkingArea">Working Area on E-Thaksalawa</label>
	                    <select class="form-control" id="cmbWorkingArea" name="cmbWorkingArea"  >
	                    </select>
	                </div>

	                <div class="form-group ">
	                    <label for="cmbQualification">Qualifications</label>
	                    <select class="form-control" id="cmbQualification" name="cmbQualification"  >
	                    </select>
	                </div>

	                <div class="form-group">
	                    <label>Office Address</label>
	                    <input type="text" name="txtOfficeAddress" id="txtOfficeAddress" class="form-control" required/>
	                </div>

	                <div class="form-group not_mandatory">
	                    <label>School</label>
	                    <input type="text" name="txtSchool" id="txtSchool" class="form-control" required />
	                </div>

	                <div class="form-group not_mandatory">
	                    <label for="cmbEducationZone">Education Zone</label>
	                    <select class="form-control" id="cmbEducationZone" name="cmbEducationZone"  >
	                    </select>
	                </div>

	                <div class="form-group not_mandatory">
	                    <label for="cmbProvince">Province</label>
	                    <select class="form-control" id="cmbProvince" name="cmbProvince"  >
	                    </select>
	                </div>

	                <input type="button" name="submit" onclick="formValidation()" id="submit" class="btn btn-info" value="Add Person" /> 
	                <br>
	            </form>
	            
	        </div>
	        
	        <div class="col-md-3"></div>
			</div><!-- End of Add Person Form -->
	</div> <!-- End of Main Div -->
</body>
</html>

<script src="scripts/load_dropdowns.js"></script> 
<script>
	$(document).ready(function(){
	  Load_Service();
	  Load_Medium();
	  Load_Education_Zone();
	  Load_Province();
	  Load_Working_Area();
	  Load_Qualification();
	  $('.not_mandatory').hide();
	});


	$(document).ready(function () {
	    $(document).on('change', '#image_upload', function () {
	        var name = document.getElementById("image_upload").files[0].name;
	        var form_data = new FormData();
	        var ext = name.split('.').pop().toLowerCase();
	        if (jQuery.inArray(ext, ['png', 'jpg', 'jpeg']) == -1)
	        {
	            alert("Invalid Image File");
	        }
	        var oFReader = new FileReader();
	        oFReader.readAsDataURL(document.getElementById("image_upload").files[0]);
	        var f = document.getElementById("image_upload").files[0];
	        var fsize = f.size || f.fileSize;
	        if (fsize > 2000000)
	        {
	            alert("Image File Size is very big");
	        }
	        else
	        {
	            form_data.append("image_upload", document.getElementById('image_upload').files[0]);
	            $.ajax({
	                url: "image_upload.php",
	                method: "POST",
	                data: form_data,
	                contentType: false,
	                cache: false,
	                processData: false,
	                beforeSend: function () {
	                    $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
	                },
	                success: function (data)
	                {
	                    $('#uploaded_image').html(data);
	                }
	            });
	        }
	    });
	});

	
   // Submit Data
   $(document).ready(function () {
            $('#submit').click(function () {
                $('#submit').prop('disabled', true);
                var fname = $('#txtfname').val();
                var lname = $('#txtlname').val();
                var short_name = $('#txtshortname').val();
                var gender = $('#rdogender').val();
                var email = $('#txtemail').val();
                var phone = $('#txtPhone').val();
                var service = $('#cmbService').val();
                var medium = $('#cmbMedium').val();
                var grade = $('#txtGrade').val();
                var designation = $('#txtDesignation').val();
                var address = $('#txtOfficeAddress').val();
                var school = $('#txtSchool').val();
                var educationZone = $('#cmbEducationZone').val();
                var province = $('#cmbProvince').val();
                var working_area = $('#cmbWorkingArea').val();
                var qualifications = $('#cmbQualification').val();

                var ori = document.getElementById("image_upload").value;
                var path = ori.split('\\');
                document.getElementById("image_path").value = path[2];
                var image = $('#image_path').val();

               
                

                if( fname == '' || lname == '' || short_name=='' || email == '' || phone == '' || service == '0' || medium == '0' || grade == '' || designation == '' || address == ''    )  
                {
                    $('#response').html('<span class="text-danger">All Fields are required</span>')
                    $('#submit').prop('disabled', false);
                }
                else
                {
                    $.post(
                            'ajax/add_person.php ',
                            $('#person_form').serialize(),
                            function (data)
                            {
                                $('form').trigger("reset");
                                $('#response').fadeIn().html(data);
                                $('#submit').prop("disabled", false);
                                setTimeout(function () {
                                    $('#response').fadeOut("slow");
                                }, 5000);
                            }
                    );
                }
            });
        });

    $('#cmbService').change(function() {
       var service =$(this).val();
       if((service== 1) || (service== 3) || (service== 4))
       {
       	 $('.not_mandatory').show();
       }else
       {
       	 $('.not_mandatory').hide();
       }
 
    });

 </script>

 