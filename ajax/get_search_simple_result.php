<?php 
	
	include ('../dataAccess/config.php');
	
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];

	$sql = "SELECT p.personId,p.first_name,p.last_name,p.gender,p.email,p.phone,p.service_serviceId,p.medium_mediumId,p.grade,s.service_name,p.designation,p.working_area,p.qualification,p.short_name,p.school FROM person_details p,service s where  p.service_serviceId = s.serviceId AND ( p.first_name = '$fname' OR p.last_name ='$lname') "; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'personId' => $row['personId'],
   				'first_name' => $row['first_name'],
   				'last_name' => $row['last_name'],
   				'gender' => $row['gender'],
   				'email' => $row['email'],
   				'phone' => $row['phone'],
               'grade' => $row['grade'],
               'school' =>  ($row['school']==''?'None' :$row['school']),
               'service_name' => $row['service_name'],
   				'service_serviceId' => $row['service_serviceId'],
   				'medium_mediumId' => $row['medium_mediumId'],
   				'designation' => $row['designation'],
   				'working_area' => $row['working_area'],
   				'qualification' => $row['qualification'],
   				'short_name' => $row['short_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 