<?php 
	
	include ('../dataAccess/config.php');
	
	$sql = "SELECT eduzoneId,eduzone_name FROM education_zone order by eduzone_name asc"; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'eduzoneId' => $row['eduzoneId'],
   				'eduzone_name' => $row['eduzone_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 
