<?php 
	
	include ('../dataAccess/config.php');
	
	$sql = "SELECT provinceId,province_name FROM province order by province_name asc "; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'provinceId' => $row['provinceId'],
   				'province_name' => $row['province_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 
