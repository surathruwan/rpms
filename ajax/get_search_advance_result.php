<?php 
	
	include ('../dataAccess/config.php');
	
	$service = $_POST['service'];
	$province = $_POST['province'];
	$education = $_POST['education'];
	$medium = $_POST['medium'];
	$working_area=$_POST['working_area'];

	$sql = "SELECT p.personId,p.first_name,p.last_name,p.gender,p.email,p.phone,p.service_serviceId,s.service_name,p.medium_mediumId,p.grade,p.designation,p.working_area,p.qualification,p.short_name, p.province_provinceId , p.education_educationId  FROM person_details p,service s where p.service_serviceId = s.serviceId AND (p.service_serviceId = '$service' OR p.education_educationId = '$education' OR p.province_provinceId ='$province' OR p.medium_mediumId ='$medium' OR p.working_area='$working_area')"; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'personId' => $row['personId'],
   				'first_name' => $row['first_name'],
   				'last_name' => $row['last_name'],
   				'gender' => $row['gender'],
   				'email' => $row['email'],
   				'grade' => $row['grade'],
   				'service_name' => $row['service_name'],
   				'phone' => $row['phone'],
   				'school' =>  ($row['school']==''?'None' :$row['school']),
   				'service_serviceId' => $row['service_serviceId'],
   				'medium_mediumId' => $row['medium_mediumId'],
   				'designation' => $row['designation'],
   				'working_area' => $row['working_area'],
   				'qualification' => $row['qualification'],
   				'short_name' => $row['short_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 