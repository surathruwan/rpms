<?php 
	
	include ('../dataAccess/config.php');
	
	$sql = "SELECT working_id,working_name FROM working_area order by working_id asc "; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'working_id' => $row['working_id'],
   				'working_name' => $row['working_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 
