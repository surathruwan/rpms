<?php 
	
	include ('../dataAccess/config.php');
	
	$sql = "SELECT qualification_id,qualification_name FROM qualification order by qualification_id asc "; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'qualification_id' => $row['qualification_id'],
   				'qualification_name' => $row['qualification_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 
