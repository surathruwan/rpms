<?php 
	
	include ('../dataAccess/config.php');
	
	$sql = "SELECT mediumId,medium_name FROM medium  "; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'mediumId' => $row['mediumId'],
   				'medium_name' => $row['medium_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 
