<?php 
	
	include ('../dataAccess/config.php');

	$id = $_POST['id'];

	$sql = "SELECT p.personId,p.first_name,p.last_name,p.gender,p.email,p.phone,p.service_serviceId,p.medium_mediumId,p.grade,p.designation,p.official_address,p.school,p.education_educationId,p.province_provinceId,p.image,p.working_area,p.qualification,p.short_name FROM person_details p where personId = '$id'"; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			
   			$data['personId'] = $row['personId'];
   			$data['first_name'] = $row['first_name'];
   			$data['last_name'] = $row['last_name'];
   			$data['gender'] = $row['gender'];
   			$data['email'] = $row['email'];
   			$data['phone'] = $row['phone'];
   			$data['service_serviceId'] = $row['service_serviceId'];
   			$data['medium_mediumId'] = $row['medium_mediumId'];
   			$data['grade'] = $row['grade'];
   			$data['designation'] = $row['designation'];
   			$data['official_address'] = $row['official_address'];
   			$data['school'] = $row['school'];
   			$data['education_educationId'] = $row['education_educationId'];
   			$data['province_provinceId'] = $row['province_provinceId'];
   			$data['image'] = $row['image'];
            $data['working_area'] = $row['working_area'];
            $data['qualification'] = $row['qualification'];
            $data['short_name'] = $row['short_name'];
   		}

   		// header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 