
<!-- add_person.php -->
<html>
<head></head>
<!-- Styles Sheet & Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
<script src="scripts/load_dropdowns_ad_search.js"></script> 
<script src="scripts/load_dropdowns_update.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
<link rel="stylesheet" type="text/css" href="css/style.css">
<script src="scripts/table2excel.js"></script>
<!-- <script type="text/javascript" src="scripts/validate_update.js"></script> -->

<body>

	<div class="col-md-12"> <!-- Start of Main Div -->
			
			<div class="col-md-12"> <!-- Start of Heading -->
				<!-- <h3 style="text-align:center">Welcome Page</h3> -->
			</div> <!-- End of Heading -->

			<div class="col-md-12" > <!-- Start of Navigation content -->
					<div class="col-md-2 "></div>
					<div class="col-md-2 item">
					  
					</div>
					<div class="col-md-2 item">
					  <a href="Add_person.php">
						<img src="images/add-user.png" alt="" width="45%" height="auto">
					  </a>
  						<div class="item-overlay top"></div>
					</div>
					<div class="col-md-2 item">
					  <a href="view.php">
						<img src="images/search-user.png" alt="" width="45%" height="auto">
                      </a>
  						<div class="item-overlay top"></div>
					</div>
					<div class="col-md-2 item">
             
					</div>
          <div class="col-md-2 "></div>

			</div> <!-- End of Navigation content -->

			<div class="col-md-12"> <!-- Start of Search Form -->
				<div class="col-md-2"></div>
				<div class="col-md-8">
				<div class="col-md-6">
          
					<form id="simple_search">
						 <div class="form-group">
	                    <!-- <label>First Name</label> -->
	                    <input type="text" name="txtfname" id="txtfname" class="form-control" placeholder="Search by First Name" />
	                </div>

	                <div class="form-group">
	                    <!-- <label>Last Name</label> -->
	                    <input type="text" name="txtlname" id="txtlname" class="form-control" placeholder="Search by Last Name"/>
	                </div>
	            </form>
	             <input type="button" name="search" id="search" class="btn btn-info" value="Search" /> 
				</div>
				<div class="col-md-6">
					<form id="advance_search">

						<div class="form-group">
	                    <!-- <label for="cmbService">Service</label> -->
	                    <select class="form-control" id="cmbService" name="cmbService" ></select>
	                </div>

	                <div class="form-group">
	                    <!-- <label for="cmbProvince">Province</label> -->
	                    <select class="form-control" id="cmbProvince" name="cmbProvince"  ></select>
	                </div><div class="form-group">
	                    <!-- <label for="cmbEducationZone">Education Zone</label> -->
	                    <select class="form-control" id="cmbEducationZone" name="cmbEducationZone"  >
	                    </select>
	                </div>

	                 <div class="form-group">
	                    <!-- <label for="cmbMedium">Medium</label> -->
	                    <select class="form-control" id="cmbMedium" name="cmbMedium"  >
	                    </select>
	                </div>

                  <div class="form-group">
                      <select class="form-control" id="cmbWorkingArea" name="cmbWorkingArea"  >
                      </select>
                  </div>

	            </form>
	             <input type="button" name="ad_search" id="ad_search" class="btn btn-info" value="Search" /> 
				</div>
				
	           </div>
	           <div class="col-md-2"></div>
			</div><!-- End of Search Form -->


			<div class="col-md-12 table-responsive" style="margin-top:1%"><!-- Start of Table -->
				<!-- Start of Simple Search Table -->
				<table id="simple_search_result" style="display: none" class="table table-hover table-bordered full_simple_table">
		          <thead style="background: #dbe5ee">
		            <tr >
		              
		              <th data-column-id = "fname">First Name</th>
		              <th data-column-id = "lname">Last Name</th>
		              <th data-column-id = "sname">Short Name</th>
                  <th data-column-id = "school">School</th>
                  <th data-column-id = "phone">Phone</th>
                  <th data-column-id = "grade">Grade</th>
                  <th data-column-id = "service">Service</th>
		              <th ></th>
		              <th ></th>
		              <th ></th>
		            </tr>
		          </thead>

		          <tbody style="background:#fff" id="get_simple_search_result" class="full_simple_table">
		           
		          </tbody>
		        </table>
            <input class="btn btn-secondary" type="button" id="si_btnExport" value="Export" />
           
		        <!-- End of Simple Search Table -->

		        <!-- Start of Advance Search Table -->
				<table id="advance_search_result" style="display: none" class="table table-hover table-bordered">
		          <thead style="background: #dbe5ee">
		            <tr >
		              
		              <th data-column-id = "fname">First Name</th>
                  <th data-column-id = "lname">Last Name</th>
                  <th data-column-id = "sname">Short Name</th>
                  <th data-column-id = "school">School</th>
                  <th data-column-id = "phone">Phone</th>
                  <th data-column-id = "grade">Grade</th>
                  <th data-column-id = "service">Service</th>
                  <th ></th>
                  <th ></th>
                  <th ></th>
		            </tr>
		          </thead>

		          <tbody style="background:#fff" id="get_advance_search_result">
		           
		          </tbody>
		        </table>
            <input  class="btn btn-secondary" type="button" id="ad_btnExport" value="Export" />
		        <!-- End of Advance Search Table -->
			</div><!-- End of Table -->
	</div> <!-- End of Main Div -->

</body>
</html>

<div id="updateModal" class="modal fade">  
	<!-- Start point of Update Person Form -->
		<?php include_once ('includes/update_form.php'); ?>
	<!-- End point of Update Person Form -->
      
 </div>  

 <div id="viewModal" class="modal fade">  
	<!-- Start point of view Person Form -->
		<?php include_once ('includes/update_form.php'); ?>
	<!-- End point of view Person Form -->
      
 </div>  

<script type="text/javascript">
   $(document).ready(function(){
    $('#si_btnExport').hide();
    $('#ad_btnExport').hide();
	  Load_Service();
	  Load_Medium();
	  Load_Education_Zone();
	  Load_Province();
    Load_Working_Area();
	  Load_table_simple();
	  Load_table_advance();
   
	});
    // Load Table according to the first name and last name
    function Load_table_simple(){
    $('#search').click(function() {
    $('#get_simple_search_result').empty();
    document.getElementById("si_btnExport").style.display='none';
    document.getElementById("ad_btnExport").style.display='none';
    var fname=$('#txtfname').val();
    var lname=$('#txtlname').val();
    
    if((fname == "") && (lname == ""))
    {
    alert("Please Fill First Name or Last Name");
    return false;
  }else{
    document.getElementById("si_btnExport").style.display='block';
    document.getElementById("advance_search_result").style.display='none';
    document.getElementById("simple_search_result").style.display='table';
    
   

  
    

    $.ajax({
        type:"POST",
        url :"ajax/get_search_simple_result.php",
        // contentType:"application/json; charset=utf-8",
        dataType:"json",
        data: ({fname: fname , lname:lname}),
        success:function(data){
           
           var rows = '';
            
           $.each(data,function(i,item){
            
               rows += '<tr>  <td ">' +  data[i].first_name +  '</td> <td > ' + data[i].last_name + ' </td> <td > ' + data[i].short_name + ' </td><td > ' + data[i].school + ' </td> <td > ' + data[i].phone + ' </td> <td > ' + data[i].grade + ' </td> <td > ' + data[i].service_name + ' </td><td> <input type="button" name="view" id="view" data-id='+  data[i].personId +' class="btn btn-warning si_btnview" value="View" />  </td> <td> <input type="button" name="update" id="update" data-id='+  data[i].personId +' class="btn btn-info si_btnupdate" value="Update" /> </td> <td> <input type="button" name="delete" id="delete" data-id='+  data[i].personId +' data-name='+  data[i].first_name +' class="btn btn-danger si_btndelete" value="Delete" /> </td></tr>';

               console.log(rows);

           });
           $('#get_simple_search_result').append(rows);

        },
        
		});
    }
    });
   }
   
   // Load Table according to the Advance Search
   function Load_table_advance(){
    $('#ad_search').click(function() {
    $('#get_advance_search_result').empty();
    document.getElementById("si_btnExport").style.display='none';
    document.getElementById("ad_btnExport").style.display='none';
   	var service=$('#cmbService').val();
    var province=$('#cmbProvince').val();
    var education=$('#cmbEducationZone').val();
    var medium=$('#cmbMedium').val();
    var working_area=$('#cmbWorkingArea').val();
    
    if((service != "-----Select Service------") || (province != "-----Select Province------") || (education != "-----Select Education Zone------") || (medium != "-----Select Medium------") || (working_area != "-----Select Working Area------") )
    {
      
    document.getElementById("ad_btnExport").style.display='block';
    document.getElementById("simple_search_result").style.display='none';
    document.getElementById("advance_search_result").style.display='table';
   



    $.ajax({
        type:"POST",
        url :"ajax/get_search_advance_result.php",
        // contentType:"application/json; charset=utf-8",
        dataType:"json",
        data: ({service: service ,province:province,education:education,medium:medium , working_area:working_area}),
        success:function(data){
           
           var rows = '';
            
           $.each(data,function(i,item){
            
               rows += '<tr>   <td id="personName_ad">' +  data[i].first_name +  '</td> <td > ' + data[i].last_name + ' </td> <td > ' + data[i].short_name + ' </td> <td > ' + data[i].school + ' </td><td > ' + data[i].phone + ' </td><td > ' + data[i].grade + ' </td> <td > ' + data[i].service_name + ' </td> <td> <input type="button" name="view" id="view" data-id='+  data[i].personId +' class="btn btn-warning btnview" value="View" />  </td> <td> <input type="button" name="update" id="update" data-id='+  data[i].personId +' class="btn btn-info btnupdate" value="Update" /> </td> <td> <input type="button" name="delete" id="delete" data-id='+  data[i].personId +' data-name='+  data[i].first_name +' class="btn btn-danger btndelete" value="Delete" /> </td></tr>';

               console.log(rows);

           });
           $('#get_advance_search_result').append(rows);

        },
        
		});
    }
    else{
       alert("Please Select any dropdown");
      return false;
    }
    });
   }

   // Start Of Simple Search View Update Delete
   // When Click update button popup model
   $("#simple_search_result").on('click','.si_btnupdate ',function() {
   var id =  $(this).attr("data-id");  
    $.ajax({
            url: 'ajax/get_person_details.php',
            method: 'POST',
            data: { id : id },
            success: function(data) {
			      var data = $.parseJSON(data);
               // alert(data.service_serviceId);
               if((data.service_serviceId== 1) || (data.service_serviceId== 3) || (data.service_serviceId== 4))
               {
                 $('.not_mandatory').show();
               }else
               {
                 $('.not_mandatory').hide();
               }
               $("#update_txtid").val(data.personId);
               $("#update_txtfname").val(data.first_name);
               $("#update_txtlname").val(data.last_name);
               if(data.gender == 'Male')
               {
               	document.getElementById("male").checked = true;
               }else{
               	document.getElementById("female").checked = true;
               }
               $("#update_txtemail").val(data.email);
               $("#update_txtPhone").val(data.phone);
               $("#update_cmbService").val(data.service_serviceId);
               $("#update_cmbMedium").val(data.medium_mediumId);
               $("#update_txtDesignation").val(data.designation);
               $("#update_txtSchool").val(data.school);
               $("#update_txtOfficeAddress").val(data.official_address);
               $("#update_cmbEducationZone").val(data.education_educationId);
               $("#update_cmbProvince").val(data.province_provinceId);
               $("#update_txtGrade").val(data.grade);
               $("#update_txtshortname").val(data.short_name);
               $("#update_cmbWorkingArea").val(data.working_area);
               $("#update_cmbQualification").val(data.qualification);


                document["image_src"].src = "upload/"+data.image;
               enableInputFields();
               $('#updateModal').modal("show");
               Load_table_simple();

            }
          });       
    }); 


   // When Click view button popup model
   $("#simple_search_result").on('click','.si_btnview',function() {
    // var id = $('#personId').text();
     var id =  $(this).attr("data-id");
        
    $.ajax({
            url: 'ajax/get_person_details.php',
            method: 'POST',
            data: { id : id },
            success: function(data) {
			      var data = $.parseJSON(data);
               if((data.service_serviceId== 1) || (data.service_serviceId== 3) || (data.service_serviceId== 4))
               {
                 $('.not_mandatory').show();
               }else
               {
                 $('.not_mandatory').hide();
               }
               $('#updateModal').modal("show");
               $("#update_txtfname").val(data.first_name);
               $("#update_txtlname").val(data.last_name);
               if(data.gender == 'Male')
               {
               	document.getElementById("male").checked = true;
               }else{
               	document.getElementById("female").checked = true;
               }
               $("#update_txtemail").val(data.email);
               $("#update_txtPhone").val(data.phone);
               $("#update_cmbService").val(data.service_serviceId);
               $("#update_cmbMedium").val(data.medium_mediumId);
               $("#update_txtDesignation").val(data.designation);
               $("#update_txtSchool").val(data.school);
               $("#update_txtOfficeAddress").val(data.official_address);
               $("#update_cmbEducationZone").val(data.education_educationId);
               $("#update_cmbProvince").val(data.province_provinceId);
               $("#update_txtGrade").val(data.grade);
               $("#update_txtshortname").val(data.short_name);
               $("#update_cmbWorkingArea").val(data.working_area);
               $("#update_cmbQualification").val(data.qualification);
               
               document["image_src"].src = "upload/"+data.image;
               // Disable Input Fields
               disableInputFields();
               

            }
          });       
    });

    // When Click delete button popup model
    $("#simple_search_result").on('click','.si_btndelete ',function() {
    var id =  $(this).attr("data-id");
    var name =  $(this).attr("data-name"); 

    if(confirm("Are you sure you want to delete " + name + " ?")){ 
    $.ajax({
            url: 'ajax/delete_person_details.php',
            method: 'POST',
            data: { id : id },
            success: function(data) {
            Load_table_simple(); 
			  
            }
          });  
        }     
    });
    // End of Simple Search View Update Delete


    // Start Of Advance Search View Update Delete
   // When Click update button popup model
   $("#advance_search_result").on('click','.btnupdate ',function() {
   var id =  $(this).attr("data-id");    
 
    $.ajax({
            url: 'ajax/get_person_details.php',
            method: 'POST',
            data: { id : id },
            success: function(data) {
			      var data = $.parseJSON(data);
               $("#update_image_path").val(data.image);
              if((data.service_serviceId== 1) || (data.service_serviceId== 3) || (data.service_serviceId== 4))
               {
                 $('.not_mandatory').show();
               }else
               {
                 $('.not_mandatory').hide();
               }
               $("#update_txtid").val(data.personId);
               $("#update_txtfname").val(data.first_name);
               $("#update_txtlname").val(data.last_name);
               if(data.gender == 'Male')
               {
               	document.getElementById("male").checked = true;
               }else{
               	document.getElementById("female").checked = true;
               }
              
               $("#update_txtemail").val(data.email);
               $("#update_txtPhone").val(data.phone);
               $("#update_cmbService").val(data.service_serviceId);
               $("#update_cmbMedium").val(data.medium_mediumId);
               $("#update_txtDesignation").val(data.designation);
               $("#update_txtSchool").val(data.school);
               $("#update_txtOfficeAddress").val(data.official_address);
               $("#update_cmbEducationZone").val(data.education_educationId);
               $("#update_cmbProvince").val(data.province_provinceId);
               $("#update_txtGrade").val(data.grade);
               $("#update_txtshortname").val(data.short_name);
               $("#update_cmbWorkingArea").val(data.working_area);
               $("#update_cmbQualification").val(data.qualification);
                document["image_src"].src = "upload/"+data.image;
                
                
               enableInputFields();
               $('#updateModal').modal("show");
               Load_table_advance();

            }
          });       
    }); 


   // When Click view button popup model
   $("#advance_search_result").on('click','.btnview ',function() {
   var id =  $(this).attr("data-id");    
    $.ajax({
            url: 'ajax/get_person_details.php',
            method: 'POST',
            data: { id : id },
            success: function(data) {
			      var data = $.parseJSON(data);
              if((data.service_serviceId== 1) || (data.service_serviceId== 3) || (data.service_serviceId== 4))
               {
                 $('.not_mandatory').show();
               }else
               {
                 $('.not_mandatory').hide();
               }
               $("#update_txtfname").val(data.first_name);
               $("#update_txtlname").val(data.last_name);
               if(data.gender == 'Male')
               {
               	document.getElementById("male").checked = true;
               }else{
               	document.getElementById("female").checked = true;
               }
               $("#update_txtemail").val(data.email);
               $("#update_txtPhone").val(data.phone);
               $("#update_cmbService").val(data.service_serviceId);
               $("#update_cmbMedium").val(data.medium_mediumId);
               $("#update_txtDesignation").val(data.designation);
               $("#update_txtSchool").val(data.school);
               $("#update_txtOfficeAddress").val(data.official_address);
               $("#update_cmbEducationZone").val(data.education_educationId);
               $("#update_cmbProvince").val(data.province_provinceId);
               $("#update_txtGrade").val(data.grade);
               $("#update_txtshortname").val(data.short_name);
               $("#update_cmbWorkingArea").val(data.working_area);
               $("#update_cmbQualification").val(data.qualification);
               document["image_src"].src = "upload/"+data.image;

               // Disable Input Fields
               disableInputFields();
               $('#updateModal').modal("show");
               

            }
          });       
    });

    // When Click delete button popup model
    $("#advance_search_result").on('click','.btndelete ',function() {
    var id =  $(this).attr("data-id");  
    var name = $(this).attr("data-name");    
    if(confirm("Are you sure you want to delete " + name + " ?")){ 
    $.ajax({
            url: 'ajax/delete_person_details.php',
            method: 'POST',
            data: { id : id },
            success: function(data) {
             Load_table_advance();
			  
            }
          });  
        }     
    });
    // End of Advance Search View Update Delete
   
    $('#update_cmbService').change(function() {
       var service =$(this).val();
       if((service== 1) || (service== 3) || (service== 4))
       {
         $('.not_mandatory').show();
       }else
       {
         $('.not_mandatory').hide();
       }
 
    });


   function disableInputFields()
   {
	   document.getElementById("update_txtfname").disabled = true;
	   document.getElementById("update_txtlname").disabled = true; 
	   document.getElementById("update_txtemail").disabled = true;  
	   document.getElementById("update_txtPhone").disabled = true; 
	   document.getElementById("update_cmbService").disabled = true; 
	   document.getElementById("male").disabled = true; 
	   document.getElementById("female").disabled = true; 
	   document.getElementById("update_cmbMedium").disabled = true; 
	   document.getElementById("update_txtDesignation").disabled = true; 
	   document.getElementById("update_txtSchool").disabled = true; 
	   document.getElementById("update_txtOfficeAddress").disabled = true; 
	   document.getElementById("update_cmbEducationZone").disabled = true; 
	   document.getElementById("update_cmbProvince").disabled = true; 
	   document.getElementById("update_txtGrade").disabled = true;
     document.getElementById("update_cmbWorkingArea").disabled = true; 
     document.getElementById("update_cmbQualification").disabled = true; 
     document.getElementById("update_txtshortname").disabled = true; 
	   $('#model_update').hide();
	   document.getElementById("updateHeading").innerHTML = "View Person Details";
	   document.getElementById("image_upload").disabled = true; 
     document.getElementById("btnmodel_update").disabled = true; 
     $('#btnmodel_update').hide();
	  
	   
   }

   function enableInputFields()
   {
	   document.getElementById("update_txtfname").disabled = false;
	   document.getElementById("update_txtlname").disabled = false; 
	   document.getElementById("update_txtemail").disabled = false;  
	   document.getElementById("update_txtPhone").disabled = false; 
	   document.getElementById("update_cmbService").disabled = false; 
	   document.getElementById("male").disabled = false; 
	   document.getElementById("female").disabled = false; 
	   document.getElementById("update_cmbMedium").disabled = false; 
	   document.getElementById("update_txtDesignation").disabled = false; 
	   document.getElementById("update_txtSchool").disabled = false; 
	   document.getElementById("update_txtOfficeAddress").disabled = false; 
	   document.getElementById("update_cmbEducationZone").disabled = false; 
	   document.getElementById("update_cmbProvince").disabled = false; 
	   document.getElementById("update_txtGrade").disabled = false; 
	   document.getElementById("updateHeading").innerHTML = "Update Person Details";
	   document.getElementById("image_upload").disabled = false; 
     document.getElementById("update_cmbWorkingArea").disabled = false; 
     document.getElementById("update_cmbQualification").disabled = false; 
     document.getElementById("update_txtshortname").disabled = false; 
     $('#model_update').show();
     document.getElementById("btnmodel_update").disabled = false; 
     $('#btnmodel_update').show();
   }

  $(document).ready(function () {
    $(document).on('change', '#image_upload', function () {
          var name = document.getElementById("image_upload").files[0].name;
          
          document.getElementById("update_image_path").value = name;
          var form_data = new FormData();
          var ext = name.split('.').pop().toLowerCase();
          if (jQuery.inArray(ext, ['png', 'jpg', 'jpeg']) == -1)
          {
              alert("Invalid Image File");
          }
          var oFReader = new FileReader();
          oFReader.readAsDataURL(document.getElementById("image_upload").files[0]);
          var f = document.getElementById("image_upload").files[0];
          var fsize = f.size || f.fileSize;
          if (fsize > 2000000)
          {
              alert("Image File Size is very big");
          }
          else
          {
              form_data.append("image_upload", document.getElementById('image_upload').files[0]);
              $.ajax({
                  url: "image_upload.php",
                  method: "POST",
                  data: form_data,
                  contentType: false,
                  cache: false,
                  processData: false,
                  beforeSend: function () {
                      $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
                  },
                  success: function (data)
                  {
                      $('#uploaded_image').html(data);
                  }
              });
          }
      });
  });



   $(document).ready(function(){  
      $('#btnmodel_update').click(function(){ 
        var fname = $('#update_txtfname').val();
        var lname = $('#update_txtlname').val();
        var gender = $('#update_rdogender').val();
        var email = $('#update_txtemail').val();
        var phone = $('#update_txtPhone').val();
        var service = $('#update_cmbService').val();
        var medium = $('#update_cmbMedium').val();
        var grade = $('#update_txtGrade').val();
        var designation = $('#update_txtDesignation').val();
        var address = $('#update_txtOfficeAddress').val();
        var school = $('#update_txtSchool').val();
        var educationZone = $('#update_cmbEducationZone').val();
        var province = $('#update_cmbProvince').val();
        var short_name = $('#update_txtshortname').val();
        var working_area = $('#update_cmbWorkingArea').val();
        var qualifications = $('#update_cmbQualification').val();

        // var ori = document.getElementById("image_upload").value;
        // var path = ori.split('\\');
        // document.getElementById("update_image_path").value = path[2];
        var image = $('#update_image_path').val();
        var atposition = email.indexOf("@");
        var dotposition = email.lastIndexOf(".");
        
       
            if(fname == '') {
              alert("Please Fill the First Name");
              return false;
            }
            if(lname == '') {
              alert("Please Fill the Last Name");
              return false;
            }
            if(short_name == '') {
              alert("Please Fill the Short Name");
              return false;
            }
            if (gender == "") {
            alert("Please select the gender");
            return false;
            }
            
             if ((atposition < 1) || (dotposition <= atposition + 3) || (dotposition + 2 >= email.length))

            {
                alert("Not a valid e-mail address");
                return false;
            }
            if ((phone == ""))
            {
                alert("Please Fill the Mobile Number");
                // myForm.mobile.focus();
                return false;
            }

            if (isNaN(phone))
            {
                alert("Invalid Mobile Number");
                // myForm.mobile.focus();
                return false;
            }
            if (phone.length < 10)
            {
                alert("Mobile Number should be minimum 10 digits");
                // myForm.mobile.focus();
                return false;
            }
            if (service == "0") {
            alert("Please Select the Service");
            return false;
            }
            if (medium == "0") {
            alert("Please Select the Medium");
            return false;
            }
            if (grade == "") {
            alert("Please Enter the Grade");
            return false;
            }
            if (designation == "") {
            alert("Please Enter the designation");
            return false;
            }
            if (working_area == "0") {
            alert("Please Select the working area");
            return false;
            }
            if (qualifications == "0") {
            alert("Please Select the qualifications");
            return false;
            }
            if (address == "") {
            alert("Please Enter the Official Address");
            return false;
            }

            
            if ((school == "")&&((service== 1) || (service== 3) || (service== 4))) {
            alert("Please Enter the School");
            return false;
            }
            
            if ((educationZone == "0")&&((service== 1) || (service== 3) || (service== 4))) {
            alert("Please Select the Education Zone");
            return false;
            }
            
            if ((province == "0")&&((service== 1) || (service== 3) || (service== 4))) {
            alert("Please Select the Province");
            return false;
            }
            
            
        else
        {
            $.post(
                    'ajax/update_person.php ',
                    $('#update_person_form').serialize(),
                    function (data)
                    {
                        $('form').trigger("reset");
                        $('#response').fadeIn().html(data);
                        location.href = "view.php";
                        // $('#btnmodel_update').prop("disabled", false);
                        setTimeout(function () {
                            $('#response').fadeOut("slow");
                        }, 5000);
                    }
            );
        }
      }); 
 });

 $(function () {
    $("#si_btnExport").click(function () {
        var list = document.getElementsByClassName("si_btnview");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("si_btnview")[i].value=''
        }

        var list = document.getElementsByClassName("si_btnupdate");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("si_btnupdate")[i].value=''
        }

        var list = document.getElementsByClassName("si_btndelete");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("si_btndelete")[i].value=''
        }
        
        $("#simple_search_result").table2excel({
            filename: "Table.xls"
        });

        var list = document.getElementsByClassName("si_btnview");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("si_btnview")[i].value='View'
        }

        var list = document.getElementsByClassName("si_btnupdate");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("si_btnupdate")[i].value='Update'
        }

        var list = document.getElementsByClassName("si_btndelete");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("si_btndelete")[i].value='Delete'
        }

        
    });


    $("#ad_btnExport").click(function () {
        var list = document.getElementsByClassName("btnview");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("btnview")[i].value=''
        }

        var list = document.getElementsByClassName("btnupdate");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("btnupdate")[i].value=''
        }

        var list = document.getElementsByClassName("btndelete");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("btndelete")[i].value=''
        }
        
        $("#advance_search_result").table2excel({
            filename: "Table.xls"
        });

        var list = document.getElementsByClassName("btnview");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("btnview")[i].value='View'
        }

        var list = document.getElementsByClassName("btnupdate");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("btnupdate")[i].value='Update'
        }

        var list = document.getElementsByClassName("btndelete");
        for(var i = 0; i< list.length;i++){
          document.getElementsByClassName("btndelete")[i].value='Delete'
        }

        
    });
});


 </script>


