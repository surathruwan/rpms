<div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 id="updateHeading" class="modal-title"></h4>  
                </div>  
                <div class="modal-body" id="person_details" > 
                <!-- Start of Update form -->
                <form method="post" id="update_person_form" name="update_person_form" enctype="multipart/form-data">
				
					<div class="form-group">
						<span id="uploaded_image"></span>
						<br>
						<img name="image_src"  style="width:200px;height:auto;border: 2px solid #141312;"  /><br>
	                    <label >Profile Picture</label>
	                    <input type="file" name="image_upload" id="image_upload"  />
	                    <input type="hidden" name="update_image_path" id="update_image_path"  />
	                    <br />
	                    
	                </div>
	                <div class="form-group">
	                	<input type="hidden" name="update_txtid" id="update_txtid" class="form-control" required/>
	                    <label>First Name</label>
	                    <input type="text" name="update_txtfname" id="update_txtfname" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Last Name</label>
	                    <input type="text" name="update_txtlname" id="update_txtlname" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Short Name</label>
	                    <input type="text" name="update_txtshortname" id="update_txtshortname" class="form-control" required/>
	                </div>

	                <div class="form-check form-check-inline">
	                	 <label>Gender</label><br>
						 <input class="form-check-input" type="radio" name="update_rdogender" id="male" value="Male">
						 <label class="form-check-label" for="male">Male</label>
					
						 <input class="form-check-input" type="radio" name="update_rdogender" id="female" value="Female">
						 <label class="form-check-label" for="female">Female</label>
					</div>

	                <div class="form-group">
	                    <label>Email</label>
	                    <input type="email" name="update_txtemail" id="update_txtemail" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Phone</label>
	                    <input type="phone" name="update_txtPhone" id="update_txtPhone" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label for="cmbService">Service</label>
	                    <select class="form-control" id="update_cmbService" name="update_cmbService" >
	                    </select>
	                </div>

	                <div class="form-group">
	                    <label for="cmbMedium">Medium</label>
	                    <select class="form-control" id="update_cmbMedium" name="update_cmbMedium"  >
	                    </select>
	                </div>

	                <div class="form-group">
	                    <label>Grade</label>
	                    <input type="text" name="update_txtGrade" id="update_txtGrade" class="form-control" required/>
	                </div>

	                <div class="form-group">
	                    <label>Designation</label>
	                    <input type="text" name="update_txtDesignation" id="update_txtDesignation" class="form-control" required/>
	                </div>

	                <div class="form-group ">
	                    <label for="update_cmbWorkingArea">Working Area on E-Thaksalawa</label>
	                    <select class="form-control" id="update_cmbWorkingArea" name="update_cmbWorkingArea"  >
	                    </select>
	                </div>

	                <div class="form-group ">
	                    <label for="update_cmbQualification">Qualifications</label>
	                    <select class="form-control" id="update_cmbQualification" name="update_cmbQualification"  >
	                    </select>
	                </div>

	                <div class="form-group">
	                    <label>Office Address</label>
	                    <input type="text" name="update_txtOfficeAddress" id="update_txtOfficeAddress" class="form-control" required/>
	                </div>

	                <div class="form-group not_mandatory" >
	                    <label>School</label>
	                    <input type="text" name="update_txtSchool" id="update_txtSchool" class="form-control" required />
	                </div>

	                <div class="form-group not_mandatory">
	                    <label for="cmbEducationZone">Education Zone</label>
	                    <select class="form-control" id="update_cmbEducationZone" name="update_cmbEducationZone"  >
	                    </select>
	                </div>

	                <div class="form-group not_mandatory">
	                    <label for="cmbProvince">Province</label>
	                    <select class="form-control" id="update_cmbProvince" name="update_cmbProvince"  >
	                    </select>
	                </div>

	                <input type="button" name="btnmodel_update" id="btnmodel_update" class="btn btn-info" value="Update" /> 
	                <br>
	            </form>
                <!-- End of Update form  -->
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  