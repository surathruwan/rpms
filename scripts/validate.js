function formValidation() {

		var profile = document.forms["person_form"]["image_upload"].value;
        if (profile == "") {
            alert("Please Add the Profile Picture");
            return false;
        }

        var title = document.forms["person_form"]["txtfname"].value;
        if (title == "") {
            alert("Please fill the First Name");
            return false;
        }


        var fname = document.forms["person_form"]["txtlname"].value;
        if (fname == "") {
            alert("Please fill the Last Name");
            return false;
        }


        var short_name = document.forms["person_form"]["txtshortname"].value;
        if (short_name == "") {
            alert("Please fill the Short Name");
            return false;
        }

        var gender = document.forms["person_form"]["rdogender"].value;
        if (gender == "") {
            alert("Please select the gender");
            return false;
        }
        





        var email = document.forms["person_form"]["txtemail"].value;

        var atposition = email.indexOf("@");
        var dotposition = email.lastIndexOf(".");


        if ((atposition < 1) || (dotposition <= atposition + 3) || (dotposition + 2 >= email.length))

        {
            alert("Not a valid e-mail address");
            return false;
        }


       


        var mobile = document.forms["person_form"]["txtPhone"].value;
        if ((mobile == ""))
        {
            alert("Mobile Number must be filled out");
            myForm.mobile.focus();
            return false;
        }

        if (isNaN(mobile))
        {
            alert("Invalid Mobile Number");
            myForm.mobile.focus();
            return false;
        }
        if (mobile.length < 10)
        {
            alert("Mobile Number should be minimum 10 digits");
            myForm.mobile.focus();
            return false;
        }

        var service = document.forms["person_form"]["cmbService"].value;
        if (service == "-1") {
            alert("Please Select the Service");
            return false;
        }

        var medium = document.forms["person_form"]["cmbMedium"].value;
        if (medium == "-1") {
            alert("Please Select the Medium");
            return false;
        }

        var grade = document.forms["person_form"]["txtGrade"].value;
        if (grade == "") {
            alert("Please Select the Service");
            return false;
        }

        var designation = document.forms["person_form"]["txtDesignation"].value;
        if (designation == "") {
            alert("Please Select the Medium");
            return false;
        }

        var working_area = document.forms["person_form"]["cmbWorkingArea"].value;
        if (working_area == "-1") {
            alert("Please Select the Service");
            return false;
        }

        var qualification = document.forms["person_form"]["cmbQualification"].value;
        if (qualification == "-1") {
            alert("Please Select the Medium");
            return false;
        }

        var office_address = document.forms["person_form"]["txtOfficeAddress"].value;
        if (office_address == "") {
            alert("Please Select the Service");
            return false;
        }



    }


