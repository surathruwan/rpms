// Load Services function into service dropdown 
	function Load_Service(){
    $('#cmbService').empty();
    $('#cmbService').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_services.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmbService').empty();
           $('#cmbService').append("<option>-----Select Service------</option>");
           $.each(data,function(i,item){
              $('#cmbService').append('<option value="'+ data[i].serviceId +'">' + data[i].service_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

   // Load Medium function into medium dropdown 
	function Load_Medium(){
    $('#cmbMedium').empty();
    $('#cmbMedium').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_mediums.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmbMedium').empty();
           $('#cmbMedium').append("<option >-----Select Medium------</option>");
           $.each(data,function(i,item){
              $('#cmbMedium').append('<option value="'+ data[i].mediumId +'">' + data[i].medium_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }


   // Load Education Zone  function into educationZone dropdown 
	function Load_Education_Zone(){
    $('#cmbEducationZone').empty();
    $('#cmbEducationZone').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_education_zone.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmbEducationZone').empty();
           $('#cmbEducationZone').append("<option >-----Select Education Zone------</option>");
           $.each(data,function(i,item){
              $('#cmbEducationZone').append('<option value="'+ data[i].eduzoneId +'">' + data[i].eduzone_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

   // Load Province  function into province dropdown 
	function Load_Province(){
    $('#cmbProvince').empty();
    $('#cmbProvince').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_provinces.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmbProvince').empty();
           $('#cmbProvince').append("<option >-----Select Province------</option>");
           $.each(data,function(i,item){
              $('#cmbProvince').append('<option value="'+ data[i].provinceId +'">' + data[i].province_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

  // Load Working area function into Working dropdown 
  function Load_Working_Area(){
    $('#cmbWorkingArea').empty();
    $('#cmbWorkingArea').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_working_area.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmbWorkingArea').empty();
           $('#cmbWorkingArea').append("<option >-----Select Working Area------</option>");
           $.each(data,function(i,item){
              $('#cmbWorkingArea').append('<option value="'+ data[i].working_id +'">' + data[i].working_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }


  // Load Qualification function into Qualification dropdown 
  function Load_Qualification(){
    $('#cmbQualification').empty();
    $('#cmbQualification').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_qualification.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmbQualification').empty();
           $('#cmbQualification').append("<option >-----Select Qualification------</option>");
           $.each(data,function(i,item){
              $('#cmbQualification').append('<option value="'+ data[i].qualification_id +'">' + data[i].qualification_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }