$(document).ready(function(){
    Load_Service_update();
    Load_Medium_update();
    Load_Education_Zone_update();
    Load_Province_update();
    Load_Working_Area_update();
    Load_Qualification_update();
    
  });



// Load Services function into service dropdown 
	function Load_Service_update(){
    $('#update_cmbService').empty();
    $('#update_cmbService').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_services.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#update_cmbService').empty();
           $('#update_cmbService').append("<option value='0'>-----Select Service------</option>");
           $.each(data,function(i,item){
              $('#update_cmbService').append('<option value="'+ data[i].serviceId +'">' + data[i].service_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

   // Load Medium function into medium dropdown 
	function Load_Medium_update(){
    $('#update_cmbMedium').empty();
    $('#update_cmbMedium').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_mediums.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#update_cmbMedium').empty();
           $('#update_cmbMedium').append("<option value='0'>-----Select Medium------</option>");
           $.each(data,function(i,item){
              $('#update_cmbMedium').append('<option value="'+ data[i].mediumId +'">' + data[i].medium_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }


   // Load Education Zone  function into educationZone dropdown 
	function Load_Education_Zone_update(){
    $('#update_cmbEducationZone').empty();
    $('#update_cmbEducationZone').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_education_zone.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#update_cmbEducationZone').empty();
           $('#update_cmbEducationZone').append("<option value='0'>-----Select Education Zone------</option>");
           $.each(data,function(i,item){
              $('#update_cmbEducationZone').append('<option value="'+ data[i].eduzoneId +'">' + data[i].eduzone_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

   // Load Province  function into province dropdown 
	function Load_Province_update(){
    $('#update_cmbProvince').empty();
    $('#update_cmbProvince').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_provinces.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#update_cmbProvince').empty();
           $('#update_cmbProvince').append("<option value='0'>-----Select Province------</option>");
           $.each(data,function(i,item){
              $('#update_cmbProvince').append('<option value="'+ data[i].provinceId +'">' + data[i].province_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

   // Load Working area function into province dropdown 
  function Load_Working_Area_update(){
    $('#update_cmbWorkingArea').empty();
    $('#update_cmbWorkingArea').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_working_area.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#update_cmbWorkingArea').empty();
           $('#update_cmbWorkingArea').append("<option value='0'>-----Select Working Area------</option>");
           $.each(data,function(i,item){
              $('#update_cmbWorkingArea').append('<option value="'+ data[i].working_id +'">' + data[i].working_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }


  // Load Qualification function into province dropdown 
  function Load_Qualification_update(){
    $('#update_cmbQualification').empty();
    $('#update_cmbQualification').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"ajax/get_qualification.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#update_cmbQualification').empty();
           $('#update_cmbQualification').append("<option value='0'>-----Select Qualification------</option>");
           $.each(data,function(i,item){
              $('#update_cmbQualification').append('<option value="'+ data[i].qualification_id +'">' + data[i].qualification_name + '</option>');
           });
        },
        complete:function(){

        }

    });
   }
